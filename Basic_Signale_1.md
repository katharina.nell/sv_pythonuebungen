# Python Übung 1: Kennenlernen von numpy und matplotlib

## Aufgaben F-Level: 

<a name="F1"></a>

1. Plotten Sie 3 subplots untereinander mit eine Sinusfunktion (mit richtigem Zeitvektor auf der x-Achse) mit der Grundfrequenz 200 Hz, einer Samplingrate von 16000 Hz, einer Länge von 10ms und einer Amplitude von 2. Zudem eine Dreiecksfunktion mit gleichen Paramatern und eine Rechtecksfunktion mit gleichen Parametern. [Tipps für F1](#C1)

<a name="F2"></a>

2. Schreiben Sie eine Funktion, die als Übergabeparameter, die Grundfrequenz, die Samplingrate und eine List (oder numpy Array) mit den Amplituden (negative Werte sind zugelassen) der harmonischen Anteile bekommt. Bsp eine Übergabe von (100,16000,[1,-0.25,0,0.4]), sollte ergeben
$`x(k) = 1*\sin(2*\pi*kk*100/16000) -0.25*\sin(2*\pi*kk*2*100/16000) + 4*\sin(2*\pi*kk*4*100/16000)`$. Zeichnen Sie das Ergebnis für die Fourier-Reihe der ersten 7 Harmonischen eines Rechteck-Signals.


Falls Sie bei einzelnen Punkten Probleme oder Unklarheiten haben, schauen Sie beim C-Level nach.

## Aufgaben C-Level:
Nutzen Sie eine Suchmaschine (Startpage, DuckDuckGo), falls Ihnen etwas unklar ist und nutzen Sie vor allem bei den Antworten die Links zu Stack-overflow oder zur Python-Referenz. Im Folgenden finden Sie Aufteilungen und kleinere Hilfestellungen zu den F-Aufgaben.

<a name="C1">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur F-Aufgabe 1:
</a>

1. Ein digitaler Sinus is definiert als $`x(k) = A*\sin(2*\pi*kk*f_0/f_s)`$ mit der Amplitude $A$, der Frequenz$f_0$, der Abtastrate $f_s$ und der eigentlichen Laufvaribale $kk$, die eine Zeitfolge der ganzzahligen Abtastwerte beinhaltet (z.B. [0 1 2 3 4 .... ]). Starten Sie mit der Erzeugung dieses Vektors `kk = ...`.  [Tipps für C21](#L11)

[zurück zu F1](#F1)


Falls Sie bei einzelnen Punkten Probleme oder Unklarheiten haben, schauen Sie beim L-Level nach.

## Aufgaben L-Level:

<a name="L11">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur C-Aufgabe 1.1:
</a>

1. TBD1
2. TBD2. [zurück](#C1)

