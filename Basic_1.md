# Python Übung 1: Kennenlernen von numpy und matplotlib

## Aufgaben F-Level: 

<a name="F1"></a>

1. Bauen Sie ein Template Python File, das Sie für alle weiteren Übungen in der Signalverarbeitung nutzen, um Zeit zu sparen. Dieses Template importiert die oben genannten Bibliotheken mit den Kurzzeichen `np`, `plt`, `sig` und `pd`. Zusätzlich ist eine schöne (z.B. mit Achsen und Titel) plot-Routine mit den wesentlichen Start-Befehl in der Form `fig,ax = plt.subplots()` vorhanden (getestet, aber auskommentiert). [Tipps für F1](#C1)

<a name="F2"></a>

2. Plotten Sie 3 Zufallsfolgen (1 Normalverteilt mit 100 Werten, 1 gleichverteilt mit 100 Werten und gleichverteiltes Rauschen der Länge 25ms bei einer Abtastrate von 16000 Werten). [Tipps für F2](#C2)

3. (Zusatz, bzw. nächste Übung) Plotten Sie 3 subplots untereinander mit eine Sinusfunktion (mit richtigem Zeitvektor auf der x-Achse) mit der Grundfrequenz 200 Hz, einer Samplingrate von 16000 Hz, einer Länge von 10ms und einer Amplitude von 2. Zudem eine Dreiecksfunktion mit gleichen Paramatern und eine Rechtecksfunktion mit gleichen Parametern. 

Falls Sie bei einzelnen Punkten Probleme oder Unklarheiten haben, schauen Sie beim C-Level nach.

## Aufgaben C-Level:
Nutzen Sie eine Suchmaschine (Startpage, DuckDuckGo), falls Ihnen etwas unklar ist und nutzen Sie vor allem bei den Antworten die Links zu Stack-overflow oder zur Python-Referenz. Im Folgenden finden Sie Aufteilungen und kleinere Hilfestellungen zu den F-Aufgaben.

<a name="C1">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur F-Aufgabe 1:
</a>

1. Stellen Sie sicher, dass die oben genannten Bibliotheken vorhanden sind und falls nicht installieren Sie diese mit `pip`. [Tipps für C11](#L11)
2. Template bezeichnet Vorlage. Es geht also darum eine Datei `templateSV.py` irgendwo prominent zu speichern. Suchen Sie einen für Sie geeigneten Ort auf Ihrer Festplatte und speichern dort die leere Datei. [Tipps für C12](#L12)
3. Editieren Sie diese Datei. Die ersten Zeilen enthalten beschreibenden Kommentarcode (2-4 Zeilen, evtl. mehr) [Tipps für C13](#L13)
4. Dann importieren Sie die wesentlichen Bibliotheken mit den Befehlen `import` und `as` und testen ob alles richtig funktioniert, indem Sie das Skript (oder Notebook, im Weiteren wird nur vom Skript gesprochen) ausführen. [Tipps für C14](#L14)
5. Erzeugen Sie sich ein paar Daten mit `x = np.array([.......])`. [Tipps für C15](#L15)
6. Plotten Sie diese Daten. Sie brauchen eine Figure (Die eigentliche Grafik) und das eigentliche Plotfeld (Axis). Wir nutzen das Format `fig, ax = plt.subplots()`.
Plotten erfolgt mit `ax.plot(x)`. Im Skript benötigen Sie zusätzlich `plt.show()`.
[Tipps für C16](#L16)
7. Machen Sie die Grafik etwas schöner. Nutzen Sie `xlabel`, `ylabel` und `title` für Beschriftungen. [Tipps für C17](#L17)
8. Speichern Sie diese Datei und kommentieren vorher die Plotfunktionen wieder aus. Diese sollen nur helfen für zukünftige Plotaufgaben. [Tipps für C18](#L18) 

[zurück zu F1](#F1)

<a name="C2">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur F-Aufgabe 2:
</a>

1. Suchen Sie im Internet nach den Befehlen `rand` und `randn`. Wichtig bei Suchanfragen ist immer vorne den Begriff `python` mitzuverwenden. Eine weitere Suche ist die Anfrage `Erzeugen von Rauschfolgen`. Natürlich in Englisch und mit `python` davor.  [Tipps für C21](#L21)
2. Erzeugen Sie eine Grafik (1 Figure) mit 3 Plotfeldern. Die Parameter des Befehls `fig, ax = plt.subplots()` helfen hier. Suchen Sie in diesem Fall nach der Referenz für `matplotlib` oder nutzen dierekt die Hilfe, die angezeigt wird, wenn man die Klammer öffnet. Die drei Plotfelder sind als Liste in ax gespeichert. [Tipps für C22](#L22)
3. Erzeugen Sie zunächst nur 10 normalverteilte Zufallswerte und geben diese mit dem print-Befehl aus. [Tipps für C23](#L23)
4. Erzeugen Sie nun die 100 Werte und plotten diese im ersten Plotfeld `ax[0].plot`. Achtung: Python fängt immer bei 0 an zu zählen! [Tipps für C24](#L24)
5. Verfahren Sie wie im Schritt 3 und 4 mit dem nächsten Plot. [Tipps für C25](#L25)
6. Überlegen Sie sich, wie viele Werte einer bestimmten Zeitdauer in Sekunden bei einer gegebenen Abtastrate entsprechen. Programmieren Sie dies mit `len_samples = .....` und nutzen diesen neuen Wert bei der Erzeugung der Rauschfolge. [Tipps für C26](#L26)
7. Plotten Sie im 3. Plotfeld die letzte Grafik. [Tipps für C27](#L27) 

[zurück zu F2](#F2)

Aufteilung der Aufgaben und kleinere Hilfestellungen zur F-Aufgabe 3:
1. Diese wird in der nächsten Übung behandelt

Falls Sie bei einzelnen Punkten Probleme oder Unklarheiten haben, schauen Sie beim L-Level nach.

## Aufgaben L-Level:

<a name="L11">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur C-Aufgabe 1.1:
</a>

1. Die oben genannten Bibliotheken sind mit `pip install numpy matplotlib scipy pandas` leicht zu installieren. Falls Sie ihr System mit conda installiert haben, sind die vermutlich schon da. Man kann immer leicht testen, ob eine Bibliothek vorhanden ist.
    * `python` starten (in einer Kommandozeilenumgebung) und 
    * `import` Bibliotheksname eingeben.
Ist die nicht vorhanden, gibt es eine Fehlermeldung. Aus der Python-Umgebung kommt man mit `exit()`. [zurück](#C1)

<a name="L12">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur C-Aufgabe 1.2:
</a>

1. Legen Sie ein Unterverzeichnis an mit einer guten Struktur, z.B 
`User/Documents/FH/SV/python/` [zurück](#C1)

<a name="L13">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur C-Aufgabe 1.3:
</a>

1. Kommentare werden in python mit dem `#`-Zeichen angegeben.
Die ersten Code-Zeilen von allen Dateien sollten in etwas so aussehen.
(English is recommended, but not necessary). Lieber einen deutschen richtigen
Kommentar, als einen falschen englischen.
```python
# This file contains functions to do incredible things
# and a class Ring, to rule them all
# Author: Sauron_the_great@mordor.com, BSD License
# Version: 0.1.0 its not finished, but a first draft exist
```
[zurück](#C1)

<a name="L14">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur C-Aufgabe 1.4:
</a> 

1. Python ist so flexibel, da man sehr viele neue Funktionalitäten durch Module hinzuladen kann. Dies erfolgt mit dem Befehl `import`. Nach dem Import muss man zur Nutzung einzelner Funktionen immer zusätzlich den Namen des Moduls schreiben. Dies kann man abkürzen, indem man `as` verwendet. Bsp: `import numpy as np` ist ein absoluter Standard in python. 
2. Laden Sie nach diesem Beispiel auch die anderen Module nach, wobei bei `scipy` und `matplotlib` meist nur Untermodule getrennt durch einen Punkt eingeladen werden (siehe ReadMe für übliche Submodule). [zurück](#C1)

<a name="L15">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur C-Aufgabe 1.5:
</a> 

1. Numpy stellt einen neuen Datentyp für schnelle Matrixmathematik zur Verfügung. Für die digitale Signalverarbeitung ist dies optimal, da Audiodaten durch Zahlenreihen dargestellt werden können. Es gibt viele Befehle, um einfache Vektoren zu erzeugen. Schauen Sie einmal in diese Liste: https://numpy.org/doc/stable/reference/routines.array-creation.html#routines-array-creation 
und dieses Tutorial https://numpy.org/doc/stable/user/basics.creation.html 
2. Im Moment benötigen Sie nur `x = np.array([hier ein paar Zahlen mit Komma getrennt])` als Start. [zurück](#C1)

<a name="L16">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur C-Aufgabe 1.6:
</a>

1. Um Daten (`numpy-arrays`) zu plotten gibt es in python verschiedene Module. Matplotlib ist ein Standard. Für typische 2-dimensionale Daten gibt es das Modul subplots, dass in diesem Kurs ausschließlich verwendet wird. Auch hier gibt es wieder unterschiedliche Methoden es zu nutzen. Eine Methode, die besonders gut fürs Programmieren ist, funktioniert darüber, dass man Variablen hat, die die Grafik beschreiben und diese mit Daten füllen. `fig, ax = plt.subplots()` gibt die Möglichkeit die eigentliche figure, also das ganze Fenster zu verändern und ax beinhaltet die möglichen Plot-Bereiche. Wie viele es sind und wie die Anordnung ist, bestimmt man über mögliche Übergabeparameter in der Funktion (`ncols,nrows`).
2. Der eigentliche Plot-Befehl gehört zu den Plot-Bereichen, deshalb muss man `ax.plot(x)` oder `ax.plot(x,y)` verwenden, wenn man die x-Achse explizit angeben möchte.
3. In den Jupyter Notebooks reicht dies für die Darstellung. In den Skripten muss die Grafik noch gerendert werden. Dies erfolgt mit `plt.show()`. [zurück](#C1)

<a name="L17">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur C-Aufgabe 1.7:
</a>

1. Sie können ganz viele Eigenschaften der Grafik und der Plot-Bereiche ändern. Dies muss immer vor `plt.show()` erfolgen. Hier finden Sie die Liste der möglichen Befehle.
https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.html
2. Verändern Sie die x-Achsenbeschriftung mit `ax.xlabel('Zeit in s')` und verfahren genau so mit der y-Achse und dem Titel. [zurück](#C1)

<a name="L18">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur C-Aufgabe 1.8:
</a>

1. Die Plot-Funktionen benötigen Sie sehr häufig und damit diese immer zur Verfügung stehen (für copy und paste) kommentieren Sie die im Template aus.
2. Speichern Sie abschließend die Datei und Ihr Template ist fertig. [zurück](#C1)

<a name="L21">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur C-Aufgabe 2.1:
</a>

1. Numpy bietet zwei unterschiedliche Methoden Rauschen zu erzeugen. Als Start ist die Legacy Methode deutlich einfacher. Schauen Sie einmal hier:
https://numpy.org/doc/stable/reference/random/legacy.html#legacy  
Eine mögliche Suche mit dem Text `python numpy create random arrays` gibt Ihnen auch gute Hinweise.

2. Schauen Sie sich genau an, wie die Befehle `np.random.rand` und `np.random.randn` funktionieren. [zurück](#C2)

<a name="L22">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur C-Aufgabe 2.2:
</a>

1. Sie können bei dem Befehl `fig, ax = plt.subplots()` noch zusätzlich angeben, wie viele Zeilen (_rows_) und Spalten (_columns_) Sie haben möchten. Dies geschieht immer durch Nutzung der internen Parameter. Bsp: Wollen Sie 2 Zeilen und 3 Spalten haben, nutzen Sie `fig, ax = plt.subplots(ncols = 3, nrows = 2)`
2. Erzeugen Sie nun eine Grafik mit einer Spalte aber 3 Zeilen. 
3. Sie können auf die einzelnen Plot-Felder nun mit `ax[0].plot(...)` bis `ax[2].plot(...)` zugreifen. Nie vergessen: Python beginnt mit 0 zu zählen! [zurück](#C2)

<a name="L23">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur C-Aufgabe 2.3:
</a>

1. Sie können sich jederzeit ein paar Infos auf die Konsole ausgeben lassen. Angenommen Sie haben einen Vektor x mit 20 Werten, so gibt `print(x[0:10])` die ersten 10 Werte aus.
2. Erzeugen Sie mit `np.random.randn` 10 Zufallswerte und testen Sie `print`.  [zurück](#C2)

<a name="L24">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur C-Aufgabe 2.4:
</a>

1. Aus 10 macht 100. Und hier nun wichtig, eben nicht `randn(10)` zu `randn(100)` zu editieren, sondern vorher eine Variable `len_samples` für die Vektorlänge einzuführen, also `randn(len_samples)`.
2. Plotten Sie nun diesen Vektor im ersten Plotfeld `ax[0].plot`. Achtung: Python fängt immer bei 0 an zu zählen. [zurück](#C2)

<a name="L25">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur C-Aufgabe 2.5:
</a>

1. Nutzen Sie statt `randn` die Funktion für Rauschzahlen zwischen 0 und 1. Sie können den Ausgangsvektor auch sehr schön skalieren durch Multiplikation und Addition. Z.B. führt `x = 2*x-1` zu einem mittelwertfreien Vektor im Interval -1 bis 1.
2. Auch hier lohnt es sich Variablen einzuführen.
3. Plotten Sie nun im zweiten Plotfeld `ax[1]` diese neuen Daten. [zurück](#C2)

<a name="L26">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur C-Aufgabe 2.6:
</a>

1. Die Samplingrate wird in Hertz angegeben und sagt aus wie oft eine Probe (Sample) pro Sekunde vorhanden ist. Wenn Sie nun die Länge in Sekunden definieren beispielsweise `len_s = 2` für 2 Sekunden, können Sie die Anzahl der benötigten Samples direkt berechnen (Einfacher Dreisatz) `len_samples = len_s ......` .
2. Das Ergebnis ist oft eine Kommazahl vom Datentyp `float32` oder `float64`, obwohl die Zahl eine ganze Zahl ist. Viele Funktionen wie `rand` verlangen aber bei den Angaben zur Dimension echte Ganzzahlen `int`.
Sie können die Zahlentypen umwandeln, indem Sie `int(Kommazahl)` als Funktion aufrufen.
3. Erzeugen Sie nun den Rauschvektor. [zurück](#C2)

<a name="L27">
Aufteilung der Aufgaben und kleinere Hilfestellungen zur C-Aufgabe 2.7:
</a>

1. Plotten Sie im 3. Plotfeld die erzeugte Rauschfolge.  
2. Falls Sie noch Zeit haben, erzeugen Sie mit `np.linspace()` (Im Netz suchen, wie dieser Befehl zu nutzen ist), einen Zeitvektor `time_vek = np.linspace(..,...,..)` mit der gleichen Länge wie die Rauschfolge. Dieser beinhaltet die notwendigen Zeiten von 0 bis 0.025 Sekunden oder von 0 bis 25 ms.
3. Plotten Sie ins dritte Plotfeld die zeitlich richtige Darstellung mit `ax[2].plot(time_vek,x)`. [zurück](#C2)


